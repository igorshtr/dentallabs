$(document).ready(function ($) {
  /*PDP  set active first image active*/
  $(".js-pdp-image-block li:first-child").addClass("active");
  $(".js-pdp-thumbnailimage-block li:first-child").addClass("active");
  /*PDP  thumbnail click*/
  $(".js-pdp-thumbnailimage-block li").click(function (e) {
    $(".js-pdp-image-block li").removeClass("active");
    $(".js-pdp-thumbnailimage-block li").removeClass("active");
    var childIndex = parseInt($(this).index()) + 1;
    $(".js-pdp-image-block li:nth-child(" + childIndex + ")").addClass(
      "active"
    );
    $(".js-pdp-thumbnailimage-block li:nth-child(" + childIndex + ")").addClass(
      "active"
    );
  });
  /*Quantity Plus Minus*/
  $(".js-product-single__quantity .js-minus-qty").click(function () {
    var productQuantity = $(".js-quantity-selector").val();
    if (productQuantity > 1) {
      productQuantity--;
    }
    $(".js-quantity-selector").val(productQuantity);
    /* if quantity is 1, then hide the multiple people checkbox */
    if ((productQuantity = 1)) {
      $(".js-multiple-people-checbox").addClass("hide");
      $("#multiplePeople").attr("checked", false); // Unchecks it
    }
  });
  $(".js-product-single__quantity .js-plus-qty").click(function () {
    var productQuantity = $(".js-quantity-selector").val();
    productQuantity++;
    $(".js-quantity-selector").val(productQuantity);
    /* if quantity is greater then 1, then show the multiple people checkbox */
    if (productQuantity > 1) {
      $(".js-multiple-people-checbox").removeClass("hide");
    }
  });
  /* Pop up open for the shopping experience
  Insurance, addons
  */
  $(".js-popup-open").click(function () {
    window.scrollTo(0, 0);
    $("#pdp-popup").show();
  });
  /* Pop up close for the shopping experience */
  $(".js-popup-close").click(function () {
    $("#pdp-popup").hide();
  });
  
});
/* Document ready ends */




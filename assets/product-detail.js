$(document).ready(function ($) {
  /*PDP  set active first image active*/
  $(".js-pdp-image-block li:first-child").addClass("active");
  $(".js-pdp-thumbnailimage-block li:first-child").addClass("active");
  /*PDP  thumbnail click*/
  $(".js-pdp-thumbnailimage-block li").click(function (e) {
    $(".js-pdp-image-block li").removeClass("active");
    $(".js-pdp-thumbnailimage-block li").removeClass("active");
    var childIndex = parseInt($(this).index()) + 1;
    $(".js-pdp-image-block li:nth-child(" + childIndex + ")").addClass(
      "active"
    );
    $(".js-pdp-thumbnailimage-block li:nth-child(" + childIndex + ")").addClass(
      "active"
    );
  });
  /*Quantity Plus Minus*/
  $(".js-product-single__quantity .js-minus-qty").click(function () {
    var productQuantity = $(".js-quantity-selector").val();
    if (productQuantity > 1) {
      productQuantity--;
    }
    $(".js-quantity-selector").val(productQuantity);
    /* if quantity is 1, then hide the multiple people checkbox */
    if ((productQuantity = 1)) {
      $(".js-multiple-people-checbox").addClass("hide");
      $("#multiplePeople").attr("checked", false); // Unchecks it
    }
  });
  $(".js-product-single__quantity .js-plus-qty").click(function () {
    var productQuantity = $(".js-quantity-selector").val();
    productQuantity++;
    $(".js-quantity-selector").val(productQuantity);
    /* if quantity is greater then 1, then show the multiple people checkbox */
    if (productQuantity > 1) {
      $(".js-multiple-people-checbox").removeClass("hide");
    }
  });
  /* Pop up open for the shopping experience
  Insurance, addons
  */
  $(".js-popup-open").click(function () {
    window.scrollTo(0, 0);
    $("#pdp-popup").show();
  });
  /* Pop up close for the shopping experience */
  $(".js-popup-close").click(function () {
    $("#pdp-popup").hide();
  });

  /* Checkout button */
$(".js-step-click").click(function () {
  /* check if multiple people option is checked
  then show the notice to edit the quantity on the cart page */
  let multiplePeopleCheckboxChecked = false;
  if ($("#multiplePeople:checked").length > 0) {
    $(".js-multiple-people").removeClass("hide");
    multiplePeopleCheckboxChecked = true;
  } else {
    $(".js-multiple-people").addClass("hide");
  }
  var step = $(this).attr("data-attr-step");
  /* If 4th Step - Add to cart */
  if (step == "four") {
    console.log("Add to cart is clicked");
    if (sessionStorage.getItem("products")) {
      //do nothing
    } else {
      /* check if there is a disabled class for exit from the "add to cart function"*/
      if ($(this).hasClass("disabled")) {
        return;
      } else {
        /* For the main product */
        const urlParams = new URLSearchParams(window.location.search);
        const variantId = urlParams.get("variant");
        let prodId = variantId ?? $(".product-id").data("productid");
        const quantity = $(".quantity-selector").val();
        let ids = [];
        ids.push({ [prodId]: quantity });
        sessionStorage.setItem("products", JSON.stringify(ids));
      }
    }
    let addOnIds = JSON.parse(sessionStorage.getItem("addOnProducts")) ?? [];
    let main = JSON.parse(sessionStorage.getItem("products")) ?? [];
    /* main product recharge
      get the complete subscription group as there might be multiple interval options 
    */
      let ProductSellingPlanID =  $('.rc_widget__option__plans__dropdown').val();
       console.log(ProductSellingPlanID);
      if($.inArray(ProductSellingPlanID, subscriptionLib ))
      {
        console.log("success");
        ProductSellingPlanID = ProductSellingPlanID;
      }
      else{
        ProductSellingPlanID = "";
      }
    /* insurance recharge */
    let insurance = JSON.parse(sessionStorage.getItem("insurance")) ?? "";
    let InsuranceSellingPlanID = sessionStorage.getItem(
      "InsuranceSellingPlanID"
    );
    if (InsuranceSellingPlanID != "undefined") {
      InsuranceSellingPlanID = JSON.parse(InsuranceSellingPlanID) ?? "";
      InsuranceSellingPlanID = parseInt(InsuranceSellingPlanID);
    }
    else{
      InsuranceSellingPlanID = "";
    }
    // console.log(insuranceSellingPlanID);
    // console.log(addOnIds);
    // console.log(main);
    // console.log(insurance);
    let items = [];
    // Add main product.
    let key = Object.keys(main[0])[0];
    console.log(key);
    /* Check if the customer came frm the addone collection url with the token
    If yes, then add a line item Add-On: Yes
    */
    let lineProperties = {};
    lineProperties["_main-product"] = "Yes";
    // console.log(localStorage.checkAddonURLIncludeToken);
    let addonCookieValue = getCookie("addonCookie");
    console.log("addonCookieValueFromCollectionURL: " + addonCookieValue);
    if (addonCookieValue != undefined) {
      lineProperties["Add-On"] = "Yes";
    }
    // if (localStorage.checkAddonURLIncludeToken) {
    //   lineProperties["Add-On"] = "Yes";
    // }
    /* Check if the customer has previously placed any orders
    If yes, then add a line item Return Customer: Yes
    */
    if (localStorage.checkReturningCustomer) {
      lineProperties["Return Customer"] = "Yes";
    }
    /* Check if multiple people option is checked
    If yes, then add a line item Multiple People: Yes
    */
    if (multiplePeopleCheckboxChecked) {
      lineProperties["Multiple People"] = "Yes";
    }
    /* check if this product is the same as clicked through quiz then add a line item property*/
    if (localStorage.quizitem) {
      console.log(localStorage.quizitem);
      if (key == localStorage.quizitem) {
        lineProperties["_Quiz"] = "Yes";
      }
    }
    /* For the main item */
    let mainItemQuantity = parseInt(main[0][key]);
    if(ProductSellingPlanID != "")
    {
      items.push({
        id: parseInt(Object.keys(main[0])[0]),
        quantity: mainItemQuantity,
        selling_plan: ProductSellingPlanID,
        properties: lineProperties,
      });
    }
    else{
      items.push({
        id: parseInt(Object.keys(main[0])[0]),
        quantity: mainItemQuantity,
        properties: lineProperties,
      });
    }
    // Add insurance recharge product.
    if (insurance) {
      if(InsuranceSellingPlanID != "")
      {
        items.push({
          id: parseInt(insurance),
          quantity: 1,
          selling_plan: InsuranceSellingPlanID,
          // "properties": {
          //   "shipping_interval_frequency": "12",
          //   "shipping_interval_unit_type": "Months"
          // }
        });
      }
      else{
        items.push({
          id: parseInt(insurance),
          quantity: 1,
        });
      }
    }
    // Add Add-on products.
    addOnIds.forEach((elem) => {
      items.push({
        id: elem,
        quantity: 1,
      });
    });
    console.log(items);
    // Create form data.
    let formData = {
      items: items,
    };
    // Add everythig to cart.
    fetch("/cart/add.js", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    })
      .then((response) => {
        // Clear data from session.
        sessionStorage.removeItem("products");
        sessionStorage.removeItem("addOnProducts");
        sessionStorage.removeItem("insurance");
        // Redirect to cart page.
        window.location.href = "/cart";
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  } else {
    $(".pdp-steps").hide();
    $(".pdp-steps").removeClass("active");
    $(".pdp-step-" + step).show();
    $(".pdp-step-" + step).addClass("active");
    $(".pdp-step-bar li").removeClass("active");
    $(".js-pdp-step-bar-" + step).addClass("active");
    /* Step 1 - To open insurance section */
    // Product Add logic.
    if (step == "two") {
      // Clear data from session.
      sessionStorage.removeItem("products");
      sessionStorage.removeItem("addOnProducts");
      sessionStorage.removeItem("insurance");
      let varVal = $(".selector-wrapper .single-option-selector").val();
      const urlParams = new URLSearchParams(window.location.search);
      const variantId = urlParams.get("variant");
      let prodId = variantId ?? $(".product-id").data("productid");
      console.log(prodId);
      const quantity = $(".quantity-selector").val();
      let ids = [];
      ids.push({ [prodId]: quantity });
      sessionStorage.setItem("products", JSON.stringify(ids));
    }
    /* Step 2
  if insurance section - button next is clicked
  Add it to session if it has a tick class else remove it from session
  */
    if (step == "three") {
      console.log("Insurance is clicked");
      // Insurance product.
      if ($("#add-insurance").hasClass("tick-box")) {
        prodId = $("#add-insurance").data("pid");
        var InsuranceSellingPlanID = $("#add-insurance").attr(
          "data-sellingplanidInsurance"
        );
        console.log(InsuranceSellingPlanID);
        prodId = prodId.toString();
        sessionStorage.setItem("insurance", JSON.stringify(prodId));
        sessionStorage.setItem(
          "InsuranceSellingPlanID",
          JSON.stringify(InsuranceSellingPlanID)
        );
      } else {
        if (!$("#add-insurance").hasClass("active")) {
          sessionStorage.removeItem("insurance");
        }
      }
    }
    /* OLD for radio buttons below - which will not be required */
    // if($("#add-insurance").is(':checked')) {
    //   prodId = $("#add-insurance").data('pid');
    //   sessionStorage.setItem("insurance", JSON.stringify(prodId));
    // }
    // else {
    //   if(!$("#add-insurance").is(':checked')) {
    //     sessionStorage.removeItem('insurance');
    //   }
    // }
  }
});
/* Addons Click*/
$(".js-add-addon").click(function (e) {
  e.preventDefault();
  let prodId = $(this).data("prod-id");
  let addOnIds = JSON.parse(sessionStorage.getItem("addOnProducts")) ?? [];
  // If de selected, remove from session storage.
  if ($(this).hasClass("selected")) {
    $(this).removeClass("selected");
    let index = addOnIds.indexOf(prodId);
    if (index > -1) {
      addOnIds.splice(index, 1);
      window.sessionStorage.setItem(
        "addOnProducts",
        JSON.stringify(addOnIds)
      );
    }
    return;
  }
  // If selected, store pid in session storage.
  $(this).addClass("selected");
  $(this).text("added");
  addOnIds.push(prodId);
  window.sessionStorage.setItem("addOnProducts", JSON.stringify(addOnIds));
});
/* Clicking options in insurance section */
$("#add-insurance").click(function (e) {
  console.log("insurance clicked");
  if (!$("#add-insurance").hasClass("tick-box")) {
    $("#add-insurance").replaceClass("white-box tick-box");
    $("#no-insurance").replaceClass("tick-box white-box");
  }
});
$("#no-insurance").click(function (e) {
  console.log("no insurance clicked");
  if (!$("#no-insurance").hasClass("tick-box")) {
    $("#no-insurance").replaceClass("white-box tick-box");
    $("#add-insurance").replaceClass("tick-box white-box");
  }
});
(function ($) {
  $.fn.replaceClass = function (classes) {
    var allClasses = classes.split(/\s+/).slice(0, 2);
    return this.each(function () {
      $(this).toggleClass(allClasses.join(" "));
    });
  };
})(jQuery);
/* Option Selection for Free Trial and Pay onetime/subscription */
$(".js-pdp-options .js-pdp-option").click(function (e) {
  $(".js-pdp-options .js-pdp-option").removeClass("tick-box");
  $(".js-pdp-options .js-pdp-option").addClass("white-box");
  $(this).addClass("tick-box");
  $(this).removeClass("white-box");
  if ($(this).attr("data-attr") == "Free Trial") {
    $(".pdp-step-bar").hide();
    $(".js-step-click").hide();
    $(".js-free-trial").show();
    $(".js-pdp-payxxx-options").hide();
    $('.selector-wrapper').hide();
    $('.note-section').hide();
    $('.js-product-single__quantity').hide();
    $('.rc_container_wrapper').hide();
  } else {
    $(".pdp-step-bar").show();
    $(".js-step-click").show();
    $(".js-free-trial").hide();
    $(".js-pdp-payxxx-options").show();
    $('.selector-wrapper').show();
    $('.note-section').show();
    $('.js-product-single__quantity').show();
    $('.rc_container_wrapper').show();
  }
});
$(".js-pdp-pay-price").html($("#ProductPrice").html());
setTimeout(function(){  
  $(".js-pdp-pay-price").html($(".rc_widget__price--onetime").html());
/* Pay - Subscription - then hide the "no insurance" as insurance is always added with subscription */
var addInsurance=$("#add-insurance").children("h4").html();
$(".rc_widget__option input[type=radio]").click(function (e) {

  /* note: Working updated Aug 23rd 2021, now with subscription the "no insurance" option will be visible */
  $("#no-insurance").show();
  $("#add-insurance").children("h4").html(addInsurance);
  $(".js-pdp-pay-price").html($(".rc_widget__price--onetime").html());

  // if ($(this).val() == "subsave") {
  //   $("#no-insurance").hide();
  //   $("#add-insurance").removeClass("white-box");
  //   $("#add-insurance").addClass("tick-box");
  //   $("#no-insurance").removeClass("tick-box");
  //   $("#no-insurance").addClass("white-box");
  //   $("#add-insurance").children("h4").html("Protect Against Loss, Damage and More. Free with subscription");
  //   $(".js-pdp-pay-price").html($(".rc_widget__price--subsave").html());
  // } else {
  //   $("#no-insurance").show();
  //   $("#add-insurance").children("h4").html(addInsurance);

  //   $(".js-pdp-pay-price").html($(".rc_widget__price--onetime").html());
    
  // }

 
});
}, 500);
});
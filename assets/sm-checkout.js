$(document).ready(function ($) {

    /*Adding insurance box after form */
  $(".edit_checkout").children(".step__sections").append($(".js__insurance-data").html());
  $(".js__insurance-data").empty();

  /*Insurance box click storing values in session storage and redirect to insurance page  */
  $(document).on("click", ".js__add-insurance", function (e) {
    if (!$("#add-insurance").hasClass("tick-box")) {
      prodId = $("#add-insurance").data("pid");
      var InsuranceSellingPlanID = $("#add-insurance").attr(
        "data-sellingplanidInsurance"
      );
      console.log(InsuranceSellingPlanID);
      prodId = prodId.toString();
      sessionStorage.setItem("insurance", JSON.stringify(prodId));
      sessionStorage.setItem(
        "InsuranceSellingPlanID",
        JSON.stringify(InsuranceSellingPlanID)
      );
      window.location.href = "/pages/insurance";
    }
  })

    /*Insurance box click storing values in session storage and redirect to insurance page  */
    $(document).on("click", ".js__remove-insurance", function (e) {
      if ($("#add-insurance").hasClass("tick-box")) {
        prodId = $("#add-insurance").data("pid");
        var InsuranceSellingPlanID = $("#add-insurance").attr(
          "data-sellingplanidInsurance"
        );
        console.log(InsuranceSellingPlanID);
        prodId = prodId.toString();
        sessionStorage.setItem("insurance", JSON.stringify(prodId));
        sessionStorage.setItem(
          "InsuranceSellingPlanID",
          JSON.stringify(InsuranceSellingPlanID)
        );
        window.location.href = "/pages/insurance?remove";
      }
    })

  /*View insurance click and popup open*/
  $(document).on("click", ".open-insurance-popup", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $("#popup-info-insurance").show();
  });
  /*each item table if tain insurance then insurance box added tick box class  */
  $(".product-table tbody").children("tr").each(function (index) {
    
    var productName = $(this).children(".product__description").children(".product__description__name").html().toLowerCase();
    console.log(productName)
    
    if (productName.indexOf("insurance") > -1) {
      $("#add-insurance").addClass("tick-box");
      $("#add-insurance").removeClass("white-box")
      $(".js__add-insurance").hide();
      $(".js__remove-insurance").show();
      
      
    }
  })
    /*Close click and popup close*/
  $(document).on("click", ".js-popup-close", function (e) {
    $("#popup-info-insurance").hide();
  })
})

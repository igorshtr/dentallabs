$(document).ready(function ($) {
  /*
  On page load check if no items - show No items in cart else show the line  items
  */
  showGuradNotification();
  if (CartJS.cart.item_count == 0) {
    $('.empty-cart-section').show();
    $('.js-show-cart-items-section').hide();
    $('#shopify-section-cart-recommendations').hide();
  } else {
    $('.empty-cart-section').hide();
  }
});
/* Show Blue Coupon Code information of the items in the cart has a product containing "mm" */
function showGuradNotification() {
  $(".js-guard-dicount-highlight").addClass("hide");
  $(".cart-table-body").each(function (index) {
    var itemName = $(this).children(".js-title-section").children(".content").children(".item-name").html();
    if (itemName.toLowerCase().indexOf("mm") > -1) {
      $(".js-guard-dicount-highlight").removeClass("hide");
      return;
    }
  })
}
/* Remove an item */
function removeItem(variantID) {
  console.log("variantID: " + variantID);
  console.log("removeitem");
  CartJS.removeItemById(variantID, {
    "success": function (data, textStatus, jqXHR) {
      console.log('Removed!');
      console.log(data.item_count);
      console.log(data);
      console.log(variantID);
      /* remove the line items from the cart page */
      $('.cart-table-body').filter('[data-variant-id="' + variantID + '"]').remove();
      // $('.cart-table-body[data-variant-id="variantID"]').remove();
      //update the line_price and total
      bindInformation(data, variantID);
    },
    "error": function (jqXHR, textStatus, errorThrown) {
      console.log('Error: ' + errorThrown + '!');
    }
  });
}
function minusQuantity(cartThis) {
  console.log("Minus");
  let variantID = $(cartThis).attr("data-variant-id");
  console.log(variantID);
  var newQuantity = $(cartThis).closest(".cart-quantity-outer").find("input[name='updates[]']").val();
  newQuantity = parseInt(newQuantity);
  newQuantity = newQuantity - 1;
  console.log(newQuantity);
  $(cartThis).closest(".cart-quantity-outer").find("input[name='updates[]']").val(newQuantity);
  if (newQuantity == 0) {
    console.log("quantity = 0");
    removeItem(variantID);
  } else {
    CartJS.updateItemById(variantID, newQuantity, {}, {
      "success": function (data, textStatus, jqXHR) {
        console.log('quantity updated!');
        console.log(data.item_count);
        console.log(data);
        //update the line_price and total
        bindInformation(data, variantID);
      },
      "error": function (jqXHR, textStatus, errorThrown) {
        console.log('Error: ' + errorThrown + '!');
      }
    });
    //CartJS.updateItemById(lineIndex, newQuantity);
  }
};
function plusQuantity(cartThis) {
  console.log("plus");
  let variantID = $(cartThis).attr("data-variant-id");
  console.log(variantID);
  var newQuantity = $(cartThis).closest(".cart-quantity-outer").find("input[name='updates[]']").val();
  newQuantity = parseInt(newQuantity);
  newQuantity = newQuantity + 1;
  console.log(newQuantity);
  $(cartThis).closest(".cart-quantity-outer").find("input[name='updates[]']").val(newQuantity);
  CartJS.updateItemById(variantID, newQuantity, {}, {
    "success": function (data, textStatus, jqXHR) {
      console.log('quantity updated!');
      console.log(data.item_count);
      console.log(data);
      //update the line_price and total
      bindInformation(data, variantID);
    },
    "error": function (jqXHR, textStatus, errorThrown) {
      console.log('Error: ' + errorThrown + '!');
    }
  });
  //CartJS.updateItemById(lineIndex, newQuantity);
};
function bindInformation(data, variantID) {
  var cartTotal = data.total_price;
  cartTotal = cartTotal / 100;
  cartTotal = formatter.format(cartTotal)
  console.log("cartTotal: " + cartTotal);
  $('.js-cart-total').html(cartTotal);
  let returnDataForBind = data;
  console.log("Bind Line Item Price");
  console.log(returnDataForBind);
  console.log(variantID);
  $(returnDataForBind.items).each(function () {
    if (this.id == variantID) {
      console.log("variantID matched");
      let linePrice = this.line_price;
      linePrice = linePrice / 100;
      linePrice = formatter.format(linePrice)
      console.log(linePrice)
      $('.js-line-item-price').filter('[data-variant-id="' + variantID + '"]').text(linePrice);
    }
    /*Checking discounted item and then setting title and discounted price*/
    let originalPrice = this.original_price;
    originalPrice = originalPrice / 100;
    originalPrice = formatter.format(originalPrice)
    if (parseFloat(this.line_level_total_discount) > 0) {
      let discountedPrice = this.discounted_price;
      discountedPrice = discountedPrice / 100;
      discountedPrice = formatter.format(discountedPrice)
      console.log(this.discounted_price)
      console.log(this.original_price)
      console.log("title" + this.discounts[0].title);
      $(".cart-table-body[data-variant-id=" + this.id + "]").children(".js-price-column").html(' <s style="margin-right: 5px" class="product-single__sale-price">' + originalPrice + '</s><span class="price">' + discountedPrice + '</span><strong> ' + this.discounts[0].title + '</strong>');
    } else {
      $(".cart-table-body[data-variant-id=" + this.id + "]").children(".js-price-column").html('<span class="price">' + originalPrice + '</span>');
    }
  });
  if (returnDataForBind.item_count == 0) {
    $('.empty-cart-section').show();
    $('.js-show-cart-items-section').hide();
    $('#shopify-section-cart-recommendations').hide();
  }
  showGuradNotification();
  //reloadAjaxCartItems(data);
}
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})
